A simplified version of the Aircrack-ng suite, with Graphical User Interface, written in Qt.
It automates the Wireless Networks auditing process, 
granting any end-user the possibility to test a network for password strength.

Dependencies: Qt4 libs, aircrack-ng, qtcsv library (included in the repo)