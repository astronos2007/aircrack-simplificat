#include <QDirIterator>
#include <QFile>
#include <QFileInfo>
#include <QModelIndexList>
#include <QProcess>
#include <QSplashScreen>
#include <QStringList>
#include <QTemporaryFile>
#include <QTextStream>

#include "ui_mainwindow.h"
#include "aircrack.h"

QStringList* AircrackWindow::getWirelessInterfaces()
{
    // get the list of WLAN interfaces for the user to choose
    QStringList* wlanInterfaces = new QStringList(); // structure to hold all the interface names
    int intfNum = 0;

    QDirIterator sysClassNetIterator("/sys/class/net", QDirIterator::NoIteratorFlags);
    while (sysClassNetIterator.hasNext()) // take each directory with a net interface
    {
        // the path of each network interface is /sys/class/net/...
        QString currentInterfacePath = sysClassNetIterator.next();
        QString currentInterface = QFileInfo(currentInterfacePath).fileName();
        QString currentInterfaceUevent = currentInterfacePath + "/uevent";

        // open "uevent" and see if the interface is wireless
        QFile ueventFile(currentInterfaceUevent);
        if (!ueventFile.open(QIODevice::ReadOnly))
        {
            continue;
        }
        QTextStream ueventFileInputStream(&ueventFile);
        while (!ueventFileInputStream.atEnd())
        {
            QString ueventLine = ueventFileInputStream.readLine();
            if (ueventLine.contains("DEVTYPE=wlan"))
            {
                // the wireless interfaces have the DEVTYPE=wlan property
                wlanInterfaces->append(currentInterface);
                ++intfNum;
                break;
            }
            if (ueventLine == "")
            {
                break;
            }
        }
        ueventFile.close();
    }

    // if no wireless interfaces found, we have a problem
    if (intfNum == 0)
    {
        error("Nu există nicio interfață wireless disponibilă pentru monitorizare! Aplicația nu va funcționa în acest caz.");
        return nullptr;
    }

    return wlanInterfaces;
}

bool AircrackWindow::startMonitoringMode()
{
    // get the chosen interface
    QModelIndexList selectedList = ui->monitoringInterfacesList->selectionModel()->selectedIndexes();
    if (selectedList.empty())
    {
        warning("Nicio interfață selectată", "Selectați o interfață disponibilă din listă!");
        return false;
    }
    QString selectedInterface = selectedList.first().data().toString();
    consoleWrite("Interfață aleasă: " + selectedInterface);

    // get the physical interface name
    QString physicalInterfacePath = "/sys/class/net/" + selectedInterface + "/phy80211/name";
    QFile physicalInterfaceFile(physicalInterfacePath);
    if (!physicalInterfaceFile.open(QIODevice::ReadOnly))
    {
        error("Nu s-a putut determina numele fizic al interfeței. Reporniți aplicația și utilizați altă interfață.");
        return false;
    }
    QTextStream physicalInterfaceInputStream(&physicalInterfaceFile);
    physicalInterfaceName = physicalInterfaceInputStream.readAll();
    physicalInterfaceName = physicalInterfaceName.left(physicalInterfaceName.length() - 1);
    physicalInterfaceFile.close();

    // check if the interface has monitor mode already enabled
    QString monitoringInterfaceType;
    QString monitoringInterfaceTypePath = "/sys/class/ieee80211/" + physicalInterfaceName + "/device/net/" + selectedInterface + "/type";
    QFile monitoringInterfaceTypeFile(monitoringInterfaceTypePath);
    if (!monitoringInterfaceTypeFile.open(QIODevice::ReadOnly))
    {
        error("Nu s-a putut verifica dacă interfața curentă este deja în modul monitorizare sau nu!");
        return false;
    }
    QTextStream monitoringInterfaceInputStream(&monitoringInterfaceTypeFile);
    monitoringInterfaceType = monitoringInterfaceInputStream.readAll();
    monitoringInterfaceType = monitoringInterfaceType.left(monitoringInterfaceType.length() - 1);
    monitoringInterfaceTypeFile.close();

    if (monitoringInterfaceType == "803")
    {
        info("Deja activ", "Modul monitorizare este deja activ pentru interfața \"" + selectedInterface + "\"!");
        monitorAlreadyEnabled = true;
        monitoringInterface = selectedInterface;
        return true;
    }

    displayLoadingScreen("Se pune interfața selectată în modul monitorizare... Așteptați!");

    // put it in monitor mode, executing airmon-ng
    QStringList argumentList;
    QProcess* airmon = new QProcess(this);
    QFile airmonFile(":/modules/airmon/airmon-ng");
    QTemporaryFile* scriptFile = QTemporaryFile::createLocalFile(airmonFile);
    scriptFile->setPermissions(QFile::ReadUser | QFile::ReadGroup | QFile::ExeUser | QFile::ExeGroup | QFile::ExeOther);
    argumentList << scriptFile->fileName() << "start" << selectedInterface;
    airmon->start("/bin/sh", argumentList);
    consoleWrite("Mod monitorizare pornit: " + argumentList.join(" "));
    airmon->waitForFinished(-1);

    consoleWrite(airmon->readAllStandardOutput());
    consoleWrite(airmon->readAllStandardError());

    int airmonExitCode = airmon->exitCode();
    consoleWrite(QString::number(airmonExitCode));
    delete airmon;
    scriptFile->close();
    scriptFile->remove();
    delete scriptFile;

    switch(airmonExitCode)
    {
        case RFKILL_ERROR:
            error("Eroare RFKILL. Aplicația nu poate continua cu interfața selectată.");
            break;
        case RFKILL_UNBLOCK_ERROR:
            error("Interfața \"" + selectedInterface + "\" este blocată prin RFKILL și nu a putut fi deblocată automat. Se iese.");
            break;
        case INTERFACE_UP_ERROR:
            error("Interfața \""+ selectedInterface + "\" este dezactivată și nu a putut fi activată automat. Se iese.");
            break;
        case SUCCESS:
            break;
        default:
            error("Eroare în activarea modului monitorizare pentru interfața aleasă. Eroare de permisiuni sau de driver. Sunteți root?");
            break;
    }

    hideLoadingScreen();

    if (airmonExitCode != SUCCESS)
    {
        consoleWrite("Eroare mod monitorizare! Cod de ieșire airmon-ng: " + QString::number(airmonExitCode));
        return false;
    }

    // if the interface has more than 12 characters, it will be renamed, for compatibility
    QString newInterfaceName = selectedInterface;
    if (selectedInterface.length() > 12)
    {
        QDirIterator dirIterator("/sys/class/ieee80211/" + physicalInterfaceName + "/device/net", QDirIterator::NoIteratorFlags);
        while (dirIterator.hasNext())
        {
            QString currentDir = dirIterator.next();
            if (!currentDir.endsWith(".") && !currentDir.endsWith(".."))
            {
                newInterfaceName = QFileInfo(currentDir).fileName();
                break;
            }
        }

        // update the interfaces list
        QModelIndex selectedInterfaceIndex = selectedList.first();
        monitoringInterfacesListModel->setData(selectedInterfaceIndex, newInterfaceName);
        consoleWrite("Interfața wireless " + selectedInterface + " a fost redenumită la " + newInterfaceName);
    }
    else // otherwise, the monitoring interface will just have a "mon" prefix
    {
        newInterfaceName += "mon";
        // update the interfaces list
        QModelIndex selectedInterfaceIndex = selectedList.first();
        monitoringInterfacesListModel->setData(selectedInterfaceIndex, newInterfaceName);
    }
    monitoringInterface = newInterfaceName;
    return true;
}

void AircrackWindow::stopMonitoringMode()
{
    if (monitoringInterface.length() > 0)
    {
        displayLoadingScreen("Se oprește modul monitorizare. Așteptați...");

        QStringList argumentList;
        QProcess* airmon = new QProcess(this);
        QFile airmonFile(":/modules/airmon/airmon-ng");
        QTemporaryFile* scriptFile = QTemporaryFile::createLocalFile(airmonFile);
        scriptFile->setPermissions(QFile::ReadUser | QFile::ReadGroup | QFile::ExeUser | QFile::ExeGroup | QFile::ExeOther);
        argumentList << scriptFile->fileName() << "stop" << monitoringInterface;
        airmon->start("/bin/sh", argumentList);
        consoleWrite("Mod monitorizare oprit: " + argumentList.join(" "));
        airmon->waitForFinished(-1);
        delete airmon;
        scriptFile->close();
        scriptFile->remove();
        delete scriptFile;

        monitorAlreadyEnabled = false;
        hideLoadingScreen();
    }
}
