#include <QFile>
#include <QFileInfo>
#include <QProcess>
#include <QTemporaryFile>
#include <QThread>
#include <QTime>
#include <QTimer>
#include <csignal>
#include <unistd.h>

#include "qtcsv/variantdata.h"
#include "qtcsv/reader.h"
#include "qtcsv/writer.h"

#include "ui_mainwindow.h"
#include "aircrack.h"

void AircrackWindow::startNetworkScan()
{
    if (networkScanTimer && networkScanTimer->isActive()) // if active, we have to stop the scan
    {
        stopNetworkScan();
        return;
    }

    // clear the network list, if it exists
    if (networkListModel)
    {
        networkListModel->clear();
    }

    // remove other scan results, if any
    QFile("/tmp/netscan-01.csv").remove();

    // prepare the arguments and the command
    airodumpProcess = new QProcess();
    QFile::copy(":/modules/airodump/airodump-ng", "/tmp/airodump-ng");
    QFile airodumpFile("/tmp/airodump-ng");
    airodumpFile.setPermissions(QFile::ReadUser | QFile::ReadGroup | QFile::ReadOther | QFile::ExeUser | QFile::ExeGroup | QFile::ExeOther);
    QString command = "bash -c \"/tmp/airodump-ng -w /tmp/netscan --output-format csv --write-interval 1 " + monitoringInterface + " &>/dev/null\"";

    // start the timers
    networkScanTimer = new QTimer(this);
    networkScanElapsedTimer = new QTime();
    connect(networkScanTimer, SIGNAL(timeout()), this, SLOT(updateScanProgressBar()));
    networkScanTimer->start(100); // update the progress bar every 0.1 seconds
    networkScanElapsedTimer->start(); // measure the time, to stop when it reaches the number of seconds

    consoleWrite("Scanare rețele pornită... durată: " + QString::number(numberOfSeconds) + " secunde");

    // update the interface controls
    ui->scanForNetworksButton->setText("Oprire scanare");

    // start the background process
    airodumpProcess->startDetached(command);
    consoleWrite("Airodump pornit: " + command);
}

void AircrackWindow::updateScanProgressBar()
{
    int timeElapsed = networkScanElapsedTimer->elapsed();
    if (timeElapsed > numberOfSeconds * 1000) // time finished
    {
        // stop the scan and fetch the results
        stopNetworkScan();
        getNetworkScanResults();

        // present a message to the user to select the desired network
        if (networkListModel && networkListModel->rowCount() != 0)
        {
            ui->networkAuditingStatusMessage->setText("Dublu-click pe o rețea wireless pentru a o selecta pentru auditare.");
        }
        else
        {
            ui->networkAuditingStatusMessage->setText("Nicio rețea găsită în raza de acțiune. Încercați să scanați din nou!");
        }
        return;
    }

    // update the progress bar and fetch the results in real time
    ui->scanningProgress->setValue(timeElapsed / (10 * numberOfSeconds));
    getNetworkScanResults();
}

void AircrackWindow::stopNetworkScan()
{
    // stop the timers and interrupt the scanning process
    if (networkScanTimer)
    {
        networkScanTimer->stop();
        delete networkScanTimer;
        networkScanTimer = nullptr;
        delete networkScanElapsedTimer;
        networkScanElapsedTimer = nullptr;
    }
    consoleWrite("Scanare rețele încheiată!");

    QProcess pkill;
    pkill.execute("bash -c \"pkill -INT -f '/tmp/airodump-ng'\""); // we have to send the SIGINT signal to airodump
    if (pkill.exitCode() != 0)
    {
        error("Nu s-a putut opri scanarea din fundal! Se forțează oprirea: se va declanșa modul de monitorizare!");
        stopMonitoringMode();
        startMonitoringMode();
    }

    if (airodumpProcess)
    {
        airodumpProcess->terminate();
        delete airodumpProcess;
        airodumpProcess = nullptr;
    }

    QFile("/tmp/netscan-01.csv").remove();

    ui->scanningProgress->setValue(100);
    ui->scanForNetworksButton->setText("Scanare rețele");
    ui->networkAuditingStatusMessage->clear();

    if (networkListModel && networkListModel->rowCount() == 0)
    {
        warning("Nicio rețea găsită", "Nicio rețea găsită în raza de acțiune! Încercați să scanați din nou pentru mai mult timp.");
        networkListModel->clear();
    }
}

void AircrackWindow::getNetworkScanResults()
{
    // get the output and insert it in the list view
    QFile outputCSV("/tmp/netscan-01.csv");
    if (!outputCSV.exists())
    {
        // error("Nu s-a putut prelua lista de rețele disponibile în raza de acțiune! Încercați să scanați din nou.");
        return;
    }

    // parse the network list and show it to the user
    QList<QStringList> networkList = QtCSV::Reader::readToList(outputCSV.fileName());
    if (networkList.isEmpty()) // because the network list might be unaccessible until airodump has finished unlocking the CSV
    {
        return;
    }

    int numberOfColumns = 4;
    int numberOfRows = networkList.size();

    if (networkListModel)
    {
        delete networkListModel;
    }
    networkListModel = new QStandardItemModel(numberOfRows, numberOfColumns);

    // create the header of the table
    networkListModel->setHorizontalHeaderLabels(QStringList() << "Nume rețea" << "Semnal" << "Securitate" << "Dificultate auditare");

    // create the network list database
    if (networkListDatabase)
    {
        delete networkListDatabase;
    }
    networkListDatabase = new QList<QStringList>();

    /* BSSID, First time seen, Last time seen, channel, Speed, Privacy, Cipher, Authentication, Power, # beacons, # IV, LAN IP, ID-length, ESSID, Key
     * The items we need:
     * BSSID - column 0
     * channel - column 3
     * security protocol - column 5
     * cipher - column 6
     * authentication method - column 7
     * signal - column 8
     * number of IVs - column 10
     * ESSID - column 13
    */
    int i, networkNum = -1;
    for (i = 2; i < numberOfRows; ++i)
    {
        if (networkList.at(i).size() == 0 || (networkList.at(i).size() != 15 && networkList.at(i).size() != 7)) // skip dirty lines
        {
            continue;
        }
        if (networkList.at(i).at(0).contains("Station MAC")) // this is another section
        {
            break;
        }
        if (networkList.at(i).at(12).toInt() == 0) // zero length ESSID networks are not supported
        {
            continue;
        }

        // get the necessary information
        QString networkMAC = networkList.at(i).at(0);
        QString channel = networkList.at(i).at(3);
        QString securityProtocol = networkList.at(i).at(5);
        QString cipher = networkList.at(i).at(6);
        QString authenticationMethod = networkList.at(i).at(7);
        QString signal = networkList.at(i).at(8);
        QString numberOfBeacons = networkList.at(i).at(9);
        QString numberOfIVs = networkList.at(i).at(10);
        QString networkName = networkList.at(i).at(13);

        ++networkNum; // another network found

        // put some of the information on the user interface
        // Network name
        networkListModel->setItem(networkNum, 0, new QStandardItem(networkName));

        // Signal
        int signalNumeric = signal.toInt(0, 10);
        if (signalNumeric < -60) // weak signal
        {
            networkListModel->setItem(networkNum, 1, new QStandardItem(QIcon(":/img/wifi_weak.png"), "slab"));
        }
        else if (signalNumeric < -40) // medium signal
        {
            networkListModel->setItem(networkNum, 1, new QStandardItem(QIcon(":/img/wifi_medium.png"), "mediu"));
        }
        else
        {
            networkListModel->setItem(networkNum, 1, new QStandardItem(QIcon(":/img/wifi_strong.png"), "puternic"));
        }

        // Security protocol
        networkListModel->setItem(networkNum, 2, new QStandardItem(securityProtocol));

        // Difficulty to break
        if (authenticationMethod == "MGT") // networks with MGT (Management) authentication are not supported
        {
            networkListModel->setItem(networkNum, 3, new QStandardItem(QIcon(":/img/not_available.png"), "imposibil"));
        }
        else if (securityProtocol == "OPN") // this is an open network, you have nothing to worry about
        {
            networkListModel->setItem(networkNum, 3, new QStandardItem(QIcon(":/img/unlocked.png"), "nenecesar"));
        }
        else if (securityProtocol.contains("WEP")) // WEP, the easiest to break
        {
            networkListModel->setItem(networkNum, 3, new QStandardItem(QIcon(":/img/easy.png"), "ușor"));
        }
        else if (securityProtocol.contains("WPA") && !securityProtocol.contains("WPA2"))
        {
            networkListModel->setItem(networkNum, 3, new QStandardItem(QIcon(":/img/medium.png"), "mediu"));
        }
        else // WPA2, the hardest to break
        {
            networkListModel->setItem(networkNum, 3, new QStandardItem(QIcon(":/img/hard.png"), "greu"));
        }

        // now add an entry to the network database
        networkListDatabase->append(QStringList() << networkName << networkMAC << channel << securityProtocol
                                    << cipher << authenticationMethod << signal << numberOfBeacons << numberOfIVs);
    }

    // additional settings to the wireless network list
    networkListModel->setRowCount(networkNum + 1);
    ui->targetNetworkList->setModel(networkListModel);
    ui->targetNetworkList->setColumnWidth(0, 270);
    ui->targetNetworkList->setColumnWidth(3, 145);
    ui->targetNetworkList->horizontalHeader()->setResizeMode(QHeaderView::Fixed);
}
