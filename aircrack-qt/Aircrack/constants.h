#ifndef CONSTANTS_H
#define CONSTANTS_H

#define NEED_ROOT true // whether the application should be allowed to run even under non-root users

#define SUCCESS  0
#define FAILURE  1
#define RESTART  2

#define RFKILL_ERROR 187
#define RFKILL_UNBLOCK_ERROR 210
#define INTERFACE_UP_ERROR 211

#endif // CONSTANTS_H
