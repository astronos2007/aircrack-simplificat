#include <QFile>
#include <QTextStream>
#include <QThread>
#include <QTimer>
#include <stdio.h>

#include "ui_mainwindow.h"
#include "aircrack.h"

void AircrackWindow::startCracking()
{
    // remove other cracking results, if any
    QFile("/tmp/FAILED").remove();
    QFile("/tmp/SUCCESS").remove();
    QFile("/tmp/TIME_LEFT").remove();
    QFile("/tmp/KEYS_TESTED").remove();
    QFile("/tmp/DICT.LST").remove();

    ui->auditingProgress->setValue(100);

    // prepare the aircrack files
    aircrackProcess = new QProcess();
    QFile::copy(":/modules/aircrack/aircrack-ng", "/tmp/aircrack-ng");
    QFile aircrackFile("/tmp/aircrack-ng");
    aircrackFile.setPermissions(QFile::ReadUser | QFile::ReadGroup | QFile::ReadOther | QFile::ExeUser | QFile::ExeGroup | QFile::ExeOther);

    QString aircrackCommand;
    if (targetNetworkType == WPA_WPA2) // WPA / WPA2
    {
        QFile::copy(":/dicts/darkc0de.lst", "/tmp/DICT.LST");
        QFile dictFile("/tmp/DICT.LST");
        dictFile.setPermissions(QFile::ReadUser | QFile::ReadGroup | QFile::ReadOther | QFile::WriteUser | QFile::WriteGroup | QFile::WriteOther);

        // get the number of CPUs
        int numberOfCPUs = QThread::idealThreadCount();

        // prepare the command and the arguments
        aircrackCommand = "bash -c \"/tmp/aircrack-ng -a 2 -b " + targetNetwork->at(MAC) +
                " -p " + QString::number((numberOfCPUs == 1) ? numberOfCPUs : (numberOfCPUs / 2)) + " -w /tmp/DICT.LST /tmp/auditscancap-01.cap &>/dev/null\"";
    }
    else // WEP
    {
        aircrackCommand = "bash -c \"/tmp/aircrack-ng -a 1 -b " + targetNetwork->at(MAC) + " /tmp/auditscancap-01.cap &>/dev/null\"";

        // any signal must not intervene in the WEP cracking process
        auditingCSVTimer->blockSignals(true);
        auditingCaptureTimer->blockSignals(true);
    }

    // start the timers
    aircrackTimer = new QTimer(this);
    aircrackElapsedTimer = new QTime();
    connect(aircrackTimer, SIGNAL(timeout()), this, SLOT(updateCrackingProgress()));
    aircrackTimer->start(100); // check the status every 0.1 seconds
    aircrackElapsedTimer->start(); // measure the time, to stop when it reaches the number of seconds

    // start the background process
    consoleWrite("Proces de spargere pornit pentru rețeaua \"" + targetNetwork->at(NETWORK_NAME) + "\".");
    aircrackProcess->startDetached(aircrackCommand);
    consoleWrite("S-a pornit aircrack: " + aircrackCommand);

    // show the progress bar animated
    ui->crackingProgress->setMaximum(0);
    ui->crackingProgress->setMinimum(0);
}

void AircrackWindow::updateCrackingProgress()
{
    // get the remaining time from aircrack
    QFile timeRemainingFile("/tmp/TIME_LEFT");
    timeRemainingFile.open(QFile::ReadOnly | QFile::Text);
    QTextStream timeRemainingTextStream(&timeRemainingFile);
    int days, hours, mins, secs;
    timeRemainingTextStream >> days >> hours >> mins >> secs;
    timeRemainingTextStream.flush();
    timeRemainingFile.close();

    if (targetNetworkType == WPA_WPA2)
    {
        ui->auditingStatisticsMessage->setText("Atac în desfășurare... Timp estimativ rămas: " + QString::number(days) + " zile, "
                                           + QString::number(hours) + " ore, "
                                           + QString::number(mins) + " minute, "
                                           + QString::number(secs) + " secunde");
    }
    else
    {
        ui->auditingStatisticsMessage->setText("Atac în desfășurare... așteptați câteva minute!");
    }

    // get the success status
    QFile failedFile("/tmp/FAILED");
    QFile successFile("/tmp/SUCCESS");
    if (successFile.exists()) // cracking succeeded, get the key
    {
        successFile.open(QFile::ReadOnly | QFile::Text);
        QTextStream successFileTextStream(&successFile);
        QString key;
        successFileTextStream >> key;
        successFileTextStream.flush();
        successFile.close();

        ui->auditingStatisticsMessage->setText("Spargere reușită! [CHEIA DE SECURITATE: " + key + "]");
        ui->crackingProgress->setValue(100);
        ui->stopCrackingButton->setEnabled(false);
        triggerStep2(true);
        stopCracking();
        consoleWrite("Spargere reușită! CHEIA DE SECURITATE: " + key);
        info("Succes", "Spargere reușită! Cheia de securitate a rețelei este: " + key);
        return;
    }
    else if (failedFile.exists()) // cracking failed
    {
        stopCracking();
        if (targetNetworkType == WPA_WPA2)
        {
            ui->auditingStatisticsMessage->setText("Spargerea a eșuat, din păcate...");
            ui->crackingProgress->setValue(100);
            consoleWrite("Spargerea a eșuat...");
            warning("Eșec", "Spargerea a eșuat! Cheia de securitate a rețelei nu a putut fi găsită.");
            return;
        }
        else // if WEP, we try again with more IVs
        {
            // get the number of IVs needed
            failedFile.open(QFile::ReadOnly | QFile::Text);
            QTextStream failedFileTextStream(&failedFile);
            QString nextNumberOfIVs;
            failedFileTextStream >> nextNumberOfIVs;
            failedFile.close();
            currentIVsTarget = nextNumberOfIVs.toInt();
            ui->auditingStatisticsMessage->setText("Spargerea a eșuat, pentru moment! Se așteaptă mai mult trafic (" + nextNumberOfIVs + " de IV-uri)...");
            consoleWrite("Spargerea a eșuat, pentru moment. Se incearca peste " + nextNumberOfIVs + " IV-uri...");
        }
    }
}

void AircrackWindow::stopCracking()
{
    // stop the timers
    if (aircrackElapsedTimer)
    {
        aircrackTimer->stop();
        delete aircrackTimer;
        aircrackTimer = nullptr;
        delete aircrackElapsedTimer;
        aircrackElapsedTimer = nullptr;
    }
    consoleWrite("Aircrack încheiat!");

    if (aircrackProcess)
    {
        aircrackProcess->terminate();
        delete aircrackProcess;
        aircrackProcess = nullptr;
    }
    QProcess pkill;
    pkill.execute("bash -c \"pkill -KILL -f '/tmp/aircrack-ng'\""); // we have to send the SIGKILL signal to aircrack
    if (pkill.exitCode() != 0)
    {
        error("Could not stop the background cracking process! Forcing self-kill: monitor mode will be triggered!");
        stopMonitoringMode();
        startMonitoringMode();
    }

    ui->crackingProgress->setMaximum(100);
    QFile("/tmp/aircrack-ng").remove();

    if (targetNetworkType == WEP) // resume the WEP monitoring if cracking failed
    {
        auditingCSVTimer->blockSignals(false);
        auditingCaptureTimer->blockSignals(false);
    }
}


