#include <QApplication>
#include <QTextCodec>
#include <sys/types.h>
#include <unistd.h>
#include "aircrack.h"

int main(int argc, char *argv[])
{
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));

    // create the Aircrack application window
    QApplication a(argc, argv);
    AircrackWindow w;
    w.show();

    // first, check if the user is root or not
    unsigned int userID = getuid();
    if (NEED_ROOT && userID != 0)
    {
        w.error("Aplicația trebuie pornită ca utilizator privilegiat! Executați programul ca root.");
        return FAILURE;
    }

    return a.exec();
}
