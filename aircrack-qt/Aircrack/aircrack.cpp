#include <QAbstractItemView>
#include <QCloseEvent>
#include <QDebug>
#include <QDirIterator>
#include <QLayout>
#include <QMessageBox>
#include <QProcess>
#include <QSplashScreen>
#include <QStringListModel>
#include <QTemporaryFile>
#include <QThread>
#include <QTimer>
#include <QtConcurrentRun>

#include "aircrack.h"
#include "ui_mainwindow.h"
#include "consolewindow.h"

#include "constants.h"
#include "monitoring.cpp"
#include "utils.cpp"

AircrackWindow::AircrackWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::AircrackWindow)
{
    // variable initializations
    splash = nullptr;
    monitoringInterfacesListModel = nullptr;
    networkListModel = nullptr;
    monitoringInterface = "";
    physicalInterfaceName = "";
    networkScanTimer = nullptr;
    networkScanElapsedTimer = nullptr;
    airodumpProcess = nullptr;
    networkListDatabase = nullptr;
    targetNetwork = nullptr;
    auditingCSVTimer = nullptr;
    auditingCSVElapsedTimer = nullptr;
    auditingCSVProcess = nullptr;
    auditingCaptureTimer = nullptr;
    auditingCaptureElapsedTimer = nullptr;
    auditingCaptureProcess = nullptr;
    aireplayFakeAuthProcess = nullptr;
    aireplayARPSniffProcess = nullptr;
    targetNetworkType = NONE;
    aircrackProcess = nullptr;
    aircrackTimer = nullptr;
    aircrackElapsedTimer = nullptr;

    // create the main window
    ui->setupUi(this);
    setWindowTitle("Aircrack Simplificat");

    // create the console window
    console = new ConsoleWindow();

    // set the list views to be non-editable
    ui->monitoringInterfacesList->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->targetNetworkList->setEditTriggers(QAbstractItemView::NoEditTriggers);

    // set the network list selection mode
    ui->targetNetworkList->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->targetNetworkList->setSelectionMode(QAbstractItemView::SingleSelection);

    // choose another network button invisible
    ui->chooseAnotherNetwork->setVisible(false);

    // text messages colouring
    ui->networkAuditingStatusMessage->setStyleSheet("QLabel { color : red; }");
    ui->auditingStatisticsMessage->setStyleSheet("QLabel { color : blue; }");
}

AircrackWindow::~AircrackWindow()
{
    if (monitoringInterfacesListModel)
    {
        delete monitoringInterfacesListModel;
    }
    if (networkListModel)
    {
        delete monitoringInterfacesListModel;
    }
    if (networkScanTimer)
    {
        delete networkScanTimer;
    }
    if (networkScanElapsedTimer)
    {
        delete networkScanElapsedTimer;
    }
    if (networkListDatabase)
    {
        delete networkListDatabase;
    }
    if (airodumpProcess)
    {
        delete airodumpProcess;
    }
    if (auditingCSVTimer)
    {
        delete auditingCSVTimer;
    }
    if (auditingCSVElapsedTimer)
    {
        delete auditingCSVElapsedTimer;
    }
    if (auditingCaptureTimer)
    {
        delete auditingCaptureTimer;
    }
    if (auditingCaptureElapsedTimer)
    {
        delete auditingCaptureElapsedTimer;
    }
    if (aireplayFakeAuthProcess)
    {
        delete aireplayFakeAuthProcess;
    }
    if (aireplayARPSniffProcess)
    {
        delete aireplayARPSniffProcess;
    }
    if (aircrackProcess)
    {
        delete aircrackProcess;
    }
    if (aircrackTimer)
    {
        delete aircrackTimer;
    }
    if (aircrackElapsedTimer)
    {
        delete aircrackElapsedTimer;
    }

    delete console;
    delete ui;
}

// catch any attempt to close the window, and treat it properly
void AircrackWindow::closeEvent(QCloseEvent *event)
{
    event->ignore();
    on_actionExit_triggered();
}

void AircrackWindow::on_startButton_clicked()
{
    // activate needed controls on the interface
    ui->startButton->setEnabled(false);
    ui->stopButton->setEnabled(true);
    ui->actionReset->setEnabled(true);
    triggerStep1(true);

    consoleWrite("AIRCRACK PORNIT!");

    // update the monitoring interfaces list
    if (!monitoringInterfacesListModel)
    {
        monitoringInterfacesListModel = new QStringListModel(ui->monitoringInterfacesList);
    }

    // get the list of wireless interfaces
    QStringList* interfaceList = getWirelessInterfaces();
    if (!interfaceList)
    {
        error("Nu s-a putut prelua lista de interfețe wireless! Aplicația nu poate continua.");
        triggerStep1(false);
        return;
    }

    consoleWrite("Lista de interfețe wireless:");
    foreach (QString iface, *interfaceList)
    {
        consoleWrite("\t" + iface);
    }

    // update the interface list model
    monitoringInterfacesListModel->setStringList(*interfaceList);
    delete interfaceList;
    ui->monitoringInterfacesList->setModel(monitoringInterfacesListModel);
}

void AircrackWindow::on_monitoringInterfacesList_doubleClicked(const QModelIndex &index)
{
    consoleWrite("Utilizatorul a ales rețeaua țintă numărul " + QString::number(index.row() + 1));
    on_chooseMonitoringInterfaceButton_clicked();
}

void AircrackWindow::on_chooseMonitoringInterfaceButton_clicked()
{
    // start the monitoring mode
    bool monitorEnabled = startMonitoringMode();
    if (!monitorEnabled)
    {
        return;
    }

    // disable step 1 and enable step 2
    triggerStep1(false);
    triggerStep2(true);

    if (monitorAlreadyEnabled)
    {
        consoleWrite("Interfața wireless " + monitoringInterface + " e deja în modul monitorizare.");
    }
    else
    {
        consoleWrite("Interfața wireless " + monitoringInterface + " a fost pusă cu succes în modul monitorizare.");
        info("Mod monitorizare activat", "Interfața \"" + monitoringInterface + "\" a fost pusă cu succes în modul monitorizare.");
    }
}

void AircrackWindow::on_stopButton_clicked()
{
    if (stopDesired == false && exitDesired == false)
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Confirmare", "Sigur doriți să opriți aplicația?",
                                    QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::No)
        {
            return;
        }
    }

    // if active, we have to stop the scan
    if (networkScanTimer && networkScanTimer->isActive())
    {
        stopNetworkScan();
    }

    // if active, we have to stop auditing
    if (auditingCSVTimer && auditingCSVTimer->isActive())
    {
        stopNetworkAuditing();
    }

    // disable everything and clear all models
    ui->startButton->setEnabled(true);
    ui->stopButton->setEnabled(false);
    ui->actionReset->setEnabled(false);
    triggerStep1(false);
    triggerStep2(false);
    triggerStep3(false);

    // stop monitoring mode, if active
    stopMonitoringMode();

    // stop auditing mode, if active
    stopNetworkAuditing();

    // stop cracking, if active
    stopCracking();

    // clean up
    QFile("/tmp/aircrack-ng").remove();
    QFile("/tmp/airodump-ng").remove();
    QFile("/tmp/aireplay-ng").remove();
    QFile("/tmp/auditscancsv-01.csv").remove();
    QFile("/tmp/auditscancsv-01.kismet.csv").remove();
    QFile("/tmp/auditscancsv-01.kismet.netxml").remove();
    QFile("/tmp/auditscancap-01.ivs").remove();
    QFile("/tmp/auditscancap-01.cap").remove();
    QFile("/tmp/auditscancap-01.csv").remove();
    QFile("/tmp/auditscancap-01.kismet.csv").remove();
    QFile("/tmp/auditscancap-01.kismet.netxml").remove();
    QFile("/tmp/WPA_HANDSHAKE").remove();
    QFile("/tmp/FAILED").remove();
    QFile("/tmp/SUCCESS").remove();
    QFile("/tmp/TIME_LEFT").remove();
    QFile("/tmp/KEYS_TESTED").remove();

    consoleWrite("AIRCRACK OPRIT!");
}

void AircrackWindow::on_actionExit_triggered()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Confirmare", "Sigur doriți să părăsiți aplicația?",
                                QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
    {
        exitDesired = true;
        on_stopButton_clicked(); // exiting the application requires cleaning up
        QApplication::quit();
    }
    exitDesired = false;
}

void AircrackWindow::on_actionAbout_triggered()
{
    QString aboutMessage = QString("<h3 style='color:green; font-style: italic;'>AIRCRACK Simplificat</h3>"
                                   "<p style='color: darkblue;'>O versiune simplificată a Aircrack-ng, pentru auditarea rețelelor Wi-Fi </span><br />"
                                   "Copyright &copy; 2018 Cosmin Crihan</p>"
                                   "<p>Acest program este bazat pe o versiune modificată a suitei de utilitare <span style='color: red; font-weight: bold;'>aircrack-ng</span>."
                                   "<br />"
                                   "Toate drepturile rezervate lui Thomas d'Otreppe &lt;tdotreppe@aircrack-ng.org&gt;</p>");
    QMessageBox::about(this, "Despre", aboutMessage);
}

void AircrackWindow::on_actionShow_console_output_triggered()
{
    console->setText(consoleOutput);
    console->exec();
}

void AircrackWindow::on_actionReset_triggered()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Confirmare", "Sigur doriți să resetați aplicația?",
                                QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes)
    {
        stopDesired = true;
        reset();
    }
    stopDesired = false;
}

void AircrackWindow::on_scanForNetworksButton_clicked()
{
    // get the number of seconds to scan for
    bool validNumberOfSeconds;
    numberOfSeconds = (int)ui->secondsToScan->text().toUInt(&validNumberOfSeconds, 10);
    if (!validNumberOfSeconds)
    {
        warning("Număr invalid", "Introduceți un număr valid de secunde pentru scanare (de la 5 la 99)!");
        return;
    }
    if (numberOfSeconds < 5)
    {
        warning("Timp insuficient", "Introduceți cel puțin 5 secunde pentru a obține o scanare eficientă!");
        return;
    }

    // start the network scan
    startNetworkScan();
}

void AircrackWindow::on_targetNetworkList_doubleClicked(const QModelIndex &index)
{
    // get the network information from the database
    int selectedRow = index.row();
    QStringList* info = (QStringList*)&(networkListDatabase->at(selectedRow));

    if (info->at(AUTHENTICATION) == "MGT") // management authentication server not supported
    {
        error("Rețeaua selectată nu poate fi atacată, deoarece nu folosește chei pre-partajate (Pre-Shared Keys)!");
        return;
    }

    if (info->at(SECURITY).contains("OPN")) // open networks do not need auditing
    {
        error("Rețeaua selectată nu are protocol de securitate! Nu e nevoie de niciun atac.");
        return;
    }

    // inform the user of the difficulty and the time needed for auditing
    if (info->at(SECURITY).contains("WPA"))
    {
        QMessageBox::StandardButton reply;
        reply = QMessageBox::question(this, "Confirmare", "Sigur doriți să atacați rețeaua selectată? Procesul ar putea dura între câteva secunde și, uneori, câteva ore.",
                                    QMessageBox::Yes|QMessageBox::No);
        if (reply == QMessageBox::No)
        {
            return;
        }
    }
    else if (info->at(SECURITY).contains("WEP"))
    {
        this->warning("Atenție", "Atacarea unei rețele WEP este mai simplă, însă monitorizarea și captura de trafic ar putea dura de la câteva minute până la câteva ore.");
    }

    triggerStep2_2(true);
    ui->secondsToScan->setEnabled(false);
    ui->scanForNetworksButton->setEnabled(false);
    ui->targetNetworkList->setEnabled(false);
    ui->networkAuditingStatusMessage->setText("Apăsați pe \"Pornire auditare\" pentru a încerca să atacați rețeaua selectată.");
    consoleWrite("Informații pentru rețeaua selectată: " + info->at(NETWORK_NAME) + " | Semnal: " + info->at(SIGNAL_STRENGTH) + " | Securitate: " + info->at(SECURITY));

    // set the current network info, for monitoring
    targetNetwork = info;
}

void AircrackWindow::on_chooseAnotherNetwork_clicked()
{
    ui->secondsToScan->setEnabled(true);
    ui->targetNetworkList->setEnabled(true);
    ui->networkAuditingStatusMessage->clear();
    ui->scanForNetworksButton->setEnabled(true);
    triggerStep2_2(false);
}

void AircrackWindow::on_automaticAuditingCheck_stateChanged(int arg1)
{
    if (arg1 == Qt::Unchecked)
    {
        ui->timeForAuditingLabel->setEnabled(true);
        ui->minutesForAuditing->setEnabled(true);
        automaticMode = false;
    }
    else
    {
        ui->minutesForAuditing->clear();
        ui->timeForAuditingLabel->setEnabled(false);
        ui->minutesForAuditing->setEnabled(false);
        automaticMode = true;
    }
}

void AircrackWindow::on_startAuditingButton_clicked()
{
    // if automatic mode is disabled, check the number of minutes to be valid
    if (!automaticMode)
    {
        bool validNumberOfMinutes;
        auditingTimeLimit = ui->minutesForAuditing->text().toInt(&validNumberOfMinutes, 10);
        if (!validNumberOfMinutes)
        {
            warning("Număr invalid", "Introduceți un număr valid de minute pentru auditare (între 1 și 999)!");
            return;
        }
    }

    ui->networkAuditingStatusMessage->setText("Auditare pornită! Verificați starea în secțiunea \"Pasul 3\"!");
    ui->timeForAuditingLabel->setEnabled(false);
    ui->minutesForAuditing->setEnabled(false);
    ui->orLabel->setEnabled(false);
    ui->automaticAuditingCheck->setEnabled(false);
    ui->chooseAnotherNetwork->setEnabled(false);
    ui->startAuditingButton->setEnabled(false);
    ui->stopAuditingButton->setEnabled(true);
    ui->auditingProgress->setEnabled(true);
    ui->targetNetworkList->setEnabled(false);
    triggerStep3(true);

    // start auditing the selected network
    startNetworkAuditing();
}

void AircrackWindow::on_stopAuditingButton_clicked()
{
    ui->networkAuditingStatusMessage->setText("Apăsați pe \"Pornire auditare\" pentru a încerca să atacați rețeaua selectată.");
    ui->timeForAuditingLabel->setEnabled(true);
    ui->minutesForAuditing->setEnabled(true);
    ui->orLabel->setEnabled(true);
    ui->automaticAuditingCheck->setEnabled(true);
    ui->chooseAnotherNetwork->setEnabled(true);
    ui->startAuditingButton->setEnabled(true);
    ui->stopAuditingButton->setEnabled(false);
    ui->auditingProgress->setEnabled(false);
    triggerStep3(false);

    // clean up
    QFile("/tmp/airodump-ng").remove();
    QFile("/tmp/aireplay-ng").remove();
    QFile("/tmp/auditscancsv-01.csv").remove();
    QFile("/tmp/auditscancsv-01.kismet.csv").remove();
    QFile("/tmp/auditscancsv-01.kismet.netxml").remove();
    QFile("/tmp/auditscancap-01.ivs").remove();
    QFile("/tmp/auditscancap-01.cap").remove();
    QFile("/tmp/auditscancap-01.csv").remove();
    QFile("/tmp/auditscancap-01.kismet.csv").remove();
    QFile("/tmp/auditscancap-01.kismet.netxml").remove();
    QFile("/tmp/WPA_HANDSHAKE").remove();
    QFile("/tmp/FAILED").remove();
    QFile("/tmp/SUCCESS").remove();
    QFile("/tmp/TIME_LEFT").remove();
    QFile("/tmp/KEYS_TESTED").remove();
    QFile("/tmp/DICT.LST").remove();

    stopNetworkAuditing();
}

void AircrackWindow::on_stopCrackingButton_clicked()
{
    ui->stopCrackingButton->setEnabled(false);
    stopCracking();
    ui->auditingStatisticsMessage->setText("Spargere oprită!");
}
