#include <QMessageBox>
#include <QNetworkInterface>
#include <grp.h>
#include <pwd.h>
#include <sys/types.h>
#include <unistd.h>

#include "ui_mainwindow.h"
#include "aircrack.h"

void AircrackWindow::error(QString message)
{
    QMessageBox messageBox;
    messageBox.critical(0, "Eroare", message);
    messageBox.setFixedSize(500,200);
    messageBox.show();
}

void AircrackWindow::info(QString title, QString message)
{
    QMessageBox messageBox;
    messageBox.information(NULL, title, message);
    messageBox.setFixedSize(500,200);
    messageBox.show();
}

void AircrackWindow::warning(QString title, QString message)
{
    QMessageBox messageBox;
    messageBox.warning(NULL, title, message);
    messageBox.setFixedSize(500,200);
    messageBox.show();
}

void AircrackWindow::consoleWrite(QString message)
{
    consoleOutput.append("> " + message + "\n");
}

QString AircrackWindow::getMacAddress(QString interface)
{
    foreach(QNetworkInterface intf, QNetworkInterface::allInterfaces())
    {
        if (intf.name() == interface)
        {
            return intf.hardwareAddress();
        }
    }
    return "";
}

void AircrackWindow::reset()
{
    on_stopButton_clicked();
    ui->actionReset->setEnabled(false);
    consoleWrite("AIRCRACK RESETAT!");
}

void AircrackWindow::displayLoadingScreen(QString message)
{
    if (splash)
    {
        delete splash;
    }

    // displays a loading screen
    this->setAttribute(Qt::WA_TransparentForMouseEvents);
    QPixmap loadingSplash (":/img/splash.jpg");
    splash = new QSplashScreen(this, loadingSplash);
    splash->show();
    splash->showMessage(message, Qt::AlignCenter, Qt::black);
}

void AircrackWindow::hideLoadingScreen()
{
    if (splash)
    {
        // hides the loading screen
        this->setEnabled(true);
        // splash->finish(this);
        splash->close();
        delete splash;
        splash = nullptr;
    }
}

void AircrackWindow::triggerStep1(bool trigger)
{
    ui->step1->setEnabled(trigger);
    ui->monitoringInterfacesList->setEnabled(trigger);
    ui->chooseMonitoringInterfaceButton->setEnabled(trigger);
    ui->wirelessCard->setEnabled(trigger);
}

void AircrackWindow::triggerStep2(bool trigger)
{
    ui->step2->setEnabled(trigger);
    ui->secondsToScan->setEnabled(trigger);
    ui->scanForNetworksButton->setEnabled(trigger);
    ui->scanningProgress->setValue(0);
    ui->targetNetworkList->setEnabled(trigger);
    ui->networkAuditingStatusMessage->clear();
    ui->chooseAnotherNetwork->setEnabled(false);
    triggerStep2_2(false);

    if (trigger == false && networkListModel)
    {
        networkListModel->clear();
        networkListDatabase->clear();
    }
}

void AircrackWindow::triggerStep2_2(bool trigger)
{
    ui->chooseAnotherNetwork->setVisible(trigger);
    ui->chooseAnotherNetwork->setEnabled(trigger);
    ui->timeForAuditingLabel->setEnabled(trigger);
    ui->minutesForAuditing->setEnabled(trigger);
    ui->orLabel->setEnabled(trigger);
    ui->automaticAuditingCheck->setEnabled(trigger);
    ui->startAuditingButton->setEnabled(trigger);
    ui->stopAuditingButton->setEnabled(false);
    ui->wirelessRouter->setEnabled(trigger);
    ui->auditingProgress->setEnabled(trigger);
    ui->auditingProgress->setValue(0);
}

void AircrackWindow::triggerStep3(bool trigger)
{
    ui->step3->setEnabled(trigger);
    ui->keys->setEnabled(trigger);
    ui->stopCrackingButton->setEnabled(false);
    ui->auditingStatisticsMessage->setEnabled(trigger);
    ui->auditingStatisticsMessage->clear();
    ui->crackingResults->setEnabled(trigger);
    ui->crackingProgress->setEnabled(trigger);
    ui->crackingProgress->setValue(0);
    ui->crackingProgress->setMaximum(0);
    ui->crackingResults->clear();
}
