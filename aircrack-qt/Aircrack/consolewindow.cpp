#include "consolewindow.h"
#include "ui_consolewindow.h"

ConsoleWindow::ConsoleWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConsoleWindow)
{
    ui->setupUi(this);
}

ConsoleWindow::~ConsoleWindow()
{
    delete ui;
}

void ConsoleWindow::setText(QString& text)
{
    ui->consoleTextEdit->clear();
    ui->consoleTextEdit->appendPlainText(text);
}

void ConsoleWindow::on_closeConsoleButton_clicked()
{
    this->close();
}
