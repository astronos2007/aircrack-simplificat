#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QProcess>
#include <QSplashScreen>
#include <QStandardItemModel>
#include <QStringListModel>
#include <QTime>
#include "consolewindow.h"
#include "constants.h"

namespace Ui {
    class AircrackWindow;
}

class AircrackWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit AircrackWindow(QWidget *parent = 0);
    ~AircrackWindow();

    // message functions
    void error(QString message);
    void info(QString title, QString message);
    void warning(QString title, QString message);

    // for getting the MAC Address of the current interface
    QString getMacAddress(QString interface);

    // signal slots
private slots:
    void on_startButton_clicked();
    void on_actionExit_triggered();
    void on_stopButton_clicked();
    void on_chooseMonitoringInterfaceButton_clicked();
    void on_actionAbout_triggered();
    void on_actionShow_console_output_triggered();
    void on_actionReset_triggered();
    void on_scanForNetworksButton_clicked();
    void updateScanProgressBar();
    void csvTimerTick();
    void captureTimerTick();
    void on_targetNetworkList_doubleClicked(const QModelIndex &index);
    void on_monitoringInterfacesList_doubleClicked(const QModelIndex &index);
    void on_chooseAnotherNetwork_clicked();
    void on_stopAuditingButton_clicked();
    void on_startAuditingButton_clicked();
    void on_automaticAuditingCheck_stateChanged(int arg1);
    void on_stopCrackingButton_clicked();
    void updateCrackingProgress();

private:
    // private functions
    void reset();
    void consoleWrite(QString message);
    void displayLoadingScreen(QString message);
    void hideLoadingScreen();

    // override the close event
    void closeEvent(QCloseEvent *event);

    // monitoring mode functions
    QStringList* getWirelessInterfaces();
    bool startMonitoringMode();
    void stopMonitoringMode();

    // network listing functions
    void startNetworkScan();
    void stopNetworkScan();
    void getNetworkScanResults();

    // network auditing functions
    void startNetworkAuditing();
    void stopNetworkAuditing();

    // cracking functions
    void startCracking();
    void stopCracking();

    // interface enable / disable triggers
    void triggerStep1(bool trigger);
    void triggerStep2(bool trigger);
    void triggerStep2_2(bool trigger);
    void triggerStep3(bool trigger);

    // private variables
    Ui::AircrackWindow *ui; // user interface (main window)
    ConsoleWindow *console; // the console for debugging purposes
    QString consoleOutput;
    QSplashScreen* splash; // for any loading screen

    // booleans for control
    bool exitDesired = false;
    bool stopDesired = false;
    bool monitorAlreadyEnabled = false;
    bool wepDeauthed = false;

    // monitoring variables
    QStringListModel *monitoringInterfacesListModel;
    QString monitoringInterface; // the currently used monitoring interface
    QString physicalInterfaceName; // the physical name of the current interface

    // network scanning variables
    QStandardItemModel *networkListModel;
    QTimer* networkScanTimer; // the timer for the network scanner (ticks every 0.1 seconds)
    QTime* networkScanElapsedTimer; // for measuring the elapsed time in scanning
    int numberOfSeconds;
    QProcess* airodumpProcess;

    // the network list headers
    enum NETWORK_LIST_HEADER {
        NETWORK_NAME,
        MAC,
        CHANNEL,
        SECURITY,
        CIPHER,
        AUTHENTICATION,
        SIGNAL_STRENGTH,
        BEACONS,
        IVS
    };

    // the network list database
    QList<QStringList>* networkListDatabase;
    QStringList* targetNetwork;

    // auditing variables
    QTimer* auditingCSVTimer; // the timer for auditing and outputting to CSV (ticks every 2 seconds)
    QTime* auditingCSVElapsedTimer; // for measuring the elapsed time in auditing with CSV output
    int auditingTimeLimit;
    QTimer* auditingCaptureTimer; // the timer for auditing and outputting to a capture file (.cap) (ticks every 2 seconds)
    QTime* auditingCaptureElapsedTimer; // for measuring the elapsed time in auditing with capture (.cap) output
    bool automaticMode = false;
    QProcess* auditingCSVProcess;
    QProcess* auditingCaptureProcess;
    QProcess* aireplayFakeAuthProcess;
    QProcess* aireplayARPSniffProcess;
    int lastDeauthTime;
    int currentIVsTarget = 1000;

    // type of target network
    enum TARGET_NETWORK_TYPE {
        NONE,
        WEP,
        WPA_WPA2
    };
    TARGET_NETWORK_TYPE targetNetworkType;

    // cracking variables
    QProcess* aircrackProcess;
    QTimer* aircrackTimer;
    QTime* aircrackElapsedTimer;
};

#endif // MAINWINDOW_H
